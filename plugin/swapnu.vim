" Plugin to control numbers between relative and absolute
"
autocmd InsertLeave * :set nu rnu
autocmd InsertEnter * :set nu nornu

function! ToggleNu()
    if (&relativenumber)
        set nu nornu
    else
        set nu rnu
    endif
endfunction

map <leader>n :call ToggleNu()<CR>
