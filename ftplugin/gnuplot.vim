setl nospell

map <buffer> <leader>gs <Esc>:GnuplotStart<CR>
imap <buffer> <leader>gs <Esc>:GnuplotStart<CR>
map <buffer> <leader>pp <Esc>:GnuplotPreview<CR>
imap <buffer> <leader>pp <Esc>:GnuplotPreview<CR>
map <buffer> <leader>gp <Esc>:GnuplotBuild<CR>
imap <buffer> <leader>gp <Esc>:GnuplotBuild<CR>
