let R_exe = "octave"
let R_quit = "exit"
let R_args = ['']
let R_nvimcom_wait = -1

source ~/.vim/ftplugin/r_bindings.vim
source ~/vim-bundle/Nvim-R/ftplugin/R/global_r_plugin.vim
