map  <buffer> <LocalLeader>ch <Plug>RESendChunk
map  <buffer> <LocalLeader>cd <Plug>REDSendChunk
map  <buffer> <LocalLeader>ae G<Plug>RSendChunkFH<C-o>

map  <buffer> <LocalLeader>kd <Plug>RMakePDFK
imap <buffer> <LocalLeader>kd <Esc><Plug>RMakePDFK
map  <buffer> <LocalLeader>kn <Plug>RKnit
imap <buffer> <LocalLeader>kn <Esc><Plug>RKnit
