let R_exe = "ipython2"
let R_quit = "quit()"
let R_args = ['']
let R_nvimcom_wait = -1

source ~/.vim/ftplugin/r_bindings.vim
source ~/vim-bundle/Nvim-R/R/global_r_plugin.vim
